.. CIOP documentation master file, created by
   sphinx-quickstart on Mon Feb 25 11:42:52 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to CIOP's documentation!
================================

.. toctree::
   :maxdepth: 2 

   Overview: What is CIOP? <notebooks/overview.ipynb>
   Observations: What did we collect and where? <notebooks/observations.ipynb>
   Preliminary Results <notebooks/results.ipynb>
   People <notebooks/people.ipynb>

Contributions & Suggestions
===========================
We would like to thank several people for their contributions to this project. First, we would like to thank Stephen Griffies for recommending the name *CIOP* for our project. Second, we thank the DynOPO team not listed here, including myself, Keith Nicholls, Kurt Polzin, Eleanor Frajka-Williams, and Carl Spingys, all of whom endured the trying weather in the Southern Ocean for 2 months during 2017. Finally, we thank Mike Meredith who, although not with us, helped together with several of the aforementioned individuals provide logistics through the RRS Sir James Clark Ross (JCR). In tandem, we thank the Captain and crew of the JCR.

